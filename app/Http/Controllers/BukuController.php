<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

 
class BukuController extends Controller
{
    public function index()
    {
    	
    	$buku = DB::table('buku')->paginate(10);
 
    	
    	return view('index',['buku' => $buku]);
 
    }

    public function tambah()
    {
 
	// memanggil view tambah
	return view('tambah');
 
    }

    
    public function store(Request $request)
    {
	
	DB::table('buku')->insert([
		'buku_judul' => $request->judul,
		'buku_pengarang' => $request->pengarang,
		'tahun_terbit' => $request->tahun_terbit,
	]);
	
	return redirect('/buku');
 
    }

    public function edit($id)
    {
	
	$buku = DB::table('buku')->where('buku_id',$id)->get();
	
	return view('edit',['buku' => $buku]);
 
    }

    public function update(Request $request)
    {
	
	DB::table('buku')->where('buku_id',$request->id)->update([
		'buku_judul' => $request->judul,
		'buku_pengarang' => $request->pengarang,
		'tahun_terbit' => $request->tahun_terbit,
	]);
	
	return redirect('/buku');
    }

    public function hapus($id)
    {
	
	DB::table('buku')->where('buku_id',$id)->delete();
		
	
	return redirect('/buku');
    }
}

