<!DOCTYPE html>
<html>
<head>
	<title> CRUD Pada Laravel </title>
</head>
<body>
 
	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<h2>Rak Buku</h2>
	<a href="/buku/tambah"> + Tambah Buku Baru</a>
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Judul</th>
			<th>Pengarang</th>
			<th>Tahun Terbit</th>
			<th>Opsi</th>
		</tr>
		@foreach($buku as $b)
		<tr>
			<td>{{ $b->buku_judul }}</td>
			<td>{{ $b->buku_pengarang }}</td>
			<td>{{ $b->tahun_terbit }}</td>
			<td>
		<a href="/buku/edit/{{ $b->buku_id }}">Edit</a>
		<a onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')" 
		href="/buku/hapus/{{ $b->buku_id }}">Hapus</a>
		
	</td>	
		</tr>
		@endforeach
	</table>
 
	<br/>

	{{ $buku->links() }}

</body>
</html>

