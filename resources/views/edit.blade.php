<!DOCTYPE html>
<html>
<head>
	<title>CRUD Pada Laravel</title>
</head>
<body>
 
	<h2>Rak Buku</h2>
	
 
	<a href="/buku"> Kembali</a>
	
	<br/>
	<br/>
 
	@foreach($buku as $b)
	<form action="/buku/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $b->buku_id }}"> <br/>
		Judul <input type="text" name="judul" required="required"> <br/>
		Pengarang <input type="text" name="pengarang" required="required"> <br/>
		Tahun Terbit <input type="number" name="tahun_terbit" required="required"> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		
 
</body>
</html>