<!DOCTYPE html>
<html>
<head>
	<title>CRUD Pada Laravel </title>
</head>
<body>
 
	<h2>Rak Buku</h2>
	
 
	<a href="/buku"> Kembali</a>
	
	<br/>
	<br/>
 
	<form action="/buku/store" method="post">
		{{ csrf_field() }}
		Judul <input type="text" name="judul" required="required"> <br/>
		Pengarang <input type="text" name="pengarang" required="required"> <br/>
		Tahun Terbit <input type="number" name="tahun_terbit" required="required"> <br/>
		<input type="submit" value="Simpan Data">
	</form>
 
</body>
</html>