<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 50; $i++){

        DB::table('buku')->insert([
            'buku_judul' => $faker->bookTitle,
            'buku_pengarang' => $faker->name,
            'tahun_terbit' => $faker->numberBetween(1990,2020),
            ]);
         }
    }
}








